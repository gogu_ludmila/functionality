function api(method, url, data) {
    return new Promise(function (resolve, reject) {
        var Http = new XMLHttpRequest();
        Http.open(method, url);
        Http.send();
        Http.onreadystatechange = function (e) {
            if (Http.readyState == 4) {
                var response = JSON.parse(Http.responseText)
                if (Http.status == 200) {
                    resolve(response);
                } else {
                    reject({status: Http.status, message: response.message || 'Something was wrong'});
                }
            }
        }
    });
}

function addArticle(id, title, content){
    let article = document.createElement('div');
    article.classList.add('article');
    article.id= id;
    let h1 = document.createElement('h1');
    let titleNode = document.createTextNode(title);
    h1.appendChild(titleNode);
    let p = document.createElement('p');
    p.innerHTML = content;
    article.appendChild(h1);
    article.appendChild(p);

    let input = document.createElement("input");
    input.type = "number";
    input.min = 1;
    input.max = 5;

    let button = document.createElement('button');
    button.id = 'rate';
    button.value = 'Rate me!';
    button.appendChild(document.createTextNode('Rate'));


    let rateWrapper = document.createElement('div');
    rateWrapper.classList.add('rate-wrapper');

    rateWrapper.appendChild(input);
    rateWrapper.appendChild(button);

    article.appendChild(rateWrapper);

    return article;
}

document.addEventListener("DOMContentLoaded", function (event) {

    var url = 'http://internship.com/task/articles-api.php';

    var request = api('GET', url);

    request.then(function (response) {
        //console.log(response);
        response.forEach(function(article){
            console.log(article);
            //var n = addArticle(article.id, article.title.rendered, article.excerpt.rendered);
            var n = addArticle(article.id, article.title, article.content);
            document.getElementsByTagName('body')[0].appendChild(n);
        })
    }).catch(function (error) {
        console.log(error);
    });

});