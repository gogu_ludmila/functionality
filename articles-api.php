<?php
$articles = array(
    array(
        'id'      => 1,
        'title'   => 'Banca Mondială oferă Republicii Moldova un împrumut de 70 de milioane de dolari pentru un proiect din domeniul energetic',
        'content' => 'Banca Mondială oferă Republicii Moldova un împrumut de 70 de milioane de dolari, bani destinați sporirii capacității de distribuție a energiei electrice. Potriv...',
        'author'  => 'Natalia Volontir'
    ),
    array(
        'id'      => 2,
        'title'   => 'O companie din Turcia a primit 60 de milioane de euro pentru construcția a două drumuri din țară',
        'content' => 'Din imaginile video postate pe rețelele de socializare reiese că trei mașini au fost implicate, printre care și un taxi. În urma accidentului sunt persoane rănite. ',
        'author'  => 'Mihail Caragacean'
    ),

    array(
        'id'      => 3,
        'title'   => 'Un accident grav s-a produs noaptea trecută la Botanica! Ar fi fost implicate trei mașini, printre care și un taxi',
        'content' => 'Un drum nou de 19 km va fi construit în cadrul proiectului de reabilitare a traseului Porumbrei – Cimișlia. Acesta va avea patru benzi de circulație. Totodată, va fi construit un drum de ocolire a orașului Comrat, ce va avea peste 18 km și două benzi de circulație. Lucrările vor fi efectuate de către o companie din Turcia, iar costul acestora este aproximativ de 60 de milioane de euro.',
        'author'  => 'Mihaela Anton'
    ),

        array(
            'id'      => 4,
            'title'   => 'Jurnalistul rus Serghei Dorenko a murit într-un accident de motocicletă',
            'content' => 'Tragedia ar fi avut loc la ora 18:20 (ora Moscovei), pe strada Zemlyanoy Val, din Moscova, scrie TASS. Acesta a pierdut controlul asupra motocicletei și s-a izbit violent de un gard din beton. Medicii au constatat doar decesul jurnalistului.',
            'author'  => 'Petru Carare'
        ),

        array(
            'id'      => 5,
            'title'   => 'Atenționare de la Serviciul Vamal! Postul de tip debarcader „Cosăuți” și-a sistat temporar activitatea',
            'content' => 'Potrivit reprezentanților Vămii, motivul sistării activității punctului vamal este nivelului ridicat al apei în râul Nistru.„În aceste condiții, Serviciul Vamal recomandă persoanelor care și-au planificat traversarea frontierei prin acest post, să opteze pentru alte două posturi vamale, dislocate în proximitate: ”Soroca” (debarcader) sau ”Otaci” (rutier)”, se menționează în mesajul expediat de reprezentanții Serviciului Vamal.',
            'author'  => 'Daniela Cutu'
            )
);
echo json_encode( $articles );
exit();